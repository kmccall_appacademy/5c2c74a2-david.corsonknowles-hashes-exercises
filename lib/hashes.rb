# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  result = Hash.new
  str.split.each do |e|
    result[e] = e.length
  end
  result
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |k, v| v } [-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  result = Hash.new(0)
  word.chars.each do |e|
    result[e] += 1
  end
  result
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  check = Hash.new
  arr.each do |e|
    check[e] = true
  end
  check.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  result = Hash.new(0)
  numbers.each do |e|
    e.odd? ? result[:odd] += 1 : result[:even] += 1
  end
  result
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  hash_count = letter_counts(string)
  check = hash_count.select { |k, v| "aeiou".include?(k)}
  check.sort_by {|k, v| v } [-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  check = students.select {|k, v| v > 6}
  check.keys.combination(2)
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  specimens.uniq.length ** 2 \
  * specimens.count(specimens.min) / specimens.count(specimens.max)
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true

def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_letters = character_count(normal_sign)
  vandalized_letters = character_count(vandalized_sign)
  vandalized_letters.all? do |k, v|
    normal_letters[k] >= v
  end
end

def character_count(str)
  check = Hash.new(0)
  str.chars.each do |e|
    check[e.downcase] += 1
  end
  check
end

# def can_tweak_sign?(normal_sign, vandalized_sign)
#   p normal_sign
#   p vandalized_sign
#   normal_letters = character_count(normal_sign)
#   p "These are the normal letters"
#   p normal_letters
#
#   vandalized_letters = character_count(vandalized_sign)
#   p "These are the vandalized letters"
#   p vandalized_letters
#   vandalized_letters.each do |k, v|
#     p "This is the key value pair #{k}, #{v}"
#     p "This is the corresponding value in the other hash #{normal_letters[k]}"
#     p "this is the comparison"
#     p v > normal_letters[k.downcase]
#     if v > normal_letters[k.downcase]
#       p "this means i am going to return false"
#       return false
#     end
#   end
#   p "this means i returned true"
#   true
# end
